#include<iostream>
#include<cstdio>
#include<vector>
#include<algorithm>
#include<string.h>
#include<list>
#include<stack>
using namespace std;
/////////////////////////////////////////////////////////////////////////
int max(int a,int b)
{
	return a>b ? a : b ;
}

int min(int a,int b)
{
	return a>b ? b : a ;
}

gridInitializer(vector< vector<Node> > grid)
{
	// Sample Grid Map - How to mark locations of other bots / obstacles / turn restrictions and other constraints
	for(int i=0;i<10;i++)
		for(int j=0;j<10;j++)
			if((i+j)%3==0)
				grid[i][j].pass=false;
			else
				grid[i][j].pass=true;
}

void printPopStacks(stack<int> s1,stack<int> s2) // to print the whole path traversed , basically 
{
	while(!s1.empty() && !s2.empty())
	{
		cout<<s1.top()<<" "<<s2.top()<<endl;
		s1.pop();s2.pop();
	}
}
///////////////////////////////////////HEAP FUNCTIONS PENDING//////////////////////////////////
class Heap
{
	list< list<float> >ll;
	public:
		Heap();
		void add(vector<float> newXY);
		vector<float> pop();
		void printHeap();
		int getSize();
}

Heap::Heap()
{
	list< list<float> > llLocal(10,list<float>(10)); // 10 , 10 - an arbit- number for both dimensions  	
}

void Heap::add(list<float> newXY)
{
	if(ll.size()>0)
	{
		list<float> tmp;
		int count=0;
		while(1)
		{
			tmp=listIter.next();
			if(tmp[2]>newXY[2])
			{
				ll.add(count,newXY);break;
			}
			else
			{
				count++;
			}
			if(!listIter==ll.end())
			{
				ll.add(count,newXY);break;
			}
		}
	}
	else ll.add(newXY);
}
list<float> Heap::pop()
{
	list<float> tmp = ll.pop();
	return tmp;
}

int Heap::getSize()
{
	return  ll.size();	
}

void Heap::printHeap()
{
	list<float> tmp=listIter.next;
	bool flag=true;
	while(flag)
	{
		cout<<"Node f: "<<tmp[2]<<endl;
		if(!listIter==ll.end())
			tmp=listIter.next();
		}
		else
		{
			flag=false;
		}
	}
	cout<<"--------"<<endl;
}

/////////////////////////////////////////////////////////////////////////
class Node
{
	int x,y;
	float g,h,f;
	bool pass;
	Node *parent;
	public :
		Node(int xx,int yy);
		void updateGHFP(float gg,float hh,Node * parentt);
		bool setPass(bool passs);	
}

Node::Node(int xx,int yy)
{
	x=xx;
	y=yy;
}

void Node::updateGHFP(float gg,float hh,Node* parentt)
{
	parent=parentt;
	g=gg;
	h=hh;
	f=gg+hh;
}

bool Node::setPass(bool passs)
{
	pass=passs;
}

///////////////////////////////////////////////////////////////////////////
class JPS
{
	Grid* gridMap;
	int xMax,yMax,startX,startY,endX,endY;//xIsland,yIsland
	int dxMax,dyMax,dstartX,dstartY,dendX,dendY;
	vector<int> tmpXY;//supposed  to be an array
	vector<vector<int> >neighbors;
	float ng;
	bool draw;
	Node* tmpNode;
 	Node* cur;
	vector<Node> successors;
	vector<Node> possibleSuccess;
	list<Node> trail;
	public :
		JPS(int xMaxx,int yMaxx,vector<vector<Node> > preMadeGrid);
		void Search();
		vector<Node> identifySuccessors(Node* node);
		vector<int> jump(int x,int y,int px,int py);
		vector<int> tmpInt(int x,int y);
		vector<vector<int> > getNeighborsPrune(Node *node);
}

JPS::JPS(int xMaxx,int yMaxx,vector< vector<Node> > preMadeGrid)
{
	Grid gridMap(xMaxx,yMaxx); // calling the constructor for class Grid
	xMax=xMaxx;// x - boundary
	yMax=yMaxx;// y - boundary
	if(preMadeGrid==NULL)// No grid map supplied , rarely happens 
	{
		vector< vector<Node> > gridLocal(xMax,vector<Node> (yMax));
		gridMap->grid=gridLocal; // gridMap is the overall struct , grid is a vector< vector<Node> > instance variable to it !
	}
	else
	{
		gridMap->grid=preMadeGrid;
	}
	vector<int> startPos=gridMap.getOpenPos();// again, two randomly selected coordinates START and END 
	startX=startPos[0];
	startY=startPos[1];
	vector<int> endPos=gridMap.getOpenPos();// again, two randomly selected coordinates START and END 
	endX=endPos[0];
	endY=endPos[1];
}

void JPS::search()
{
	printf("Jump Point Search\n");
	printf("Start Point is %d %d \n ",startX,startY);
	printf("End Point is %d %d \n " ,endX,endY);
	gridMap.getNode(startX,startY).updateGHFP(0,0,NULL);// obtain the node startX,startY and call the update G,H,F,P routine on it
	gridMap.heapAdd(grid.getNode(startX,startY));//start node is added to the heap
	while(1)
	{
		cur=grid.heapPopNode();
		sx.push(cur->x);//push x - to print
		sy.push(cur->y);//push y - to print
		if(cur->x==endX && cur->y==endY)
		{
			printf("Path found ! \n ");
			printPopStacks(sx,sy);//in order to print the paths - Nodes treaded upon !
			break;
		}
		possibleSuccess=identifySuccessors(cur);//get all possible successors of the current node
		for(int i=0;i<possibleSuccess.size();i++)
		{
			if(possibleSuccess[i]!=NULL)
			{
				gridMap.heapAdd(possibleSuccess[i]);	
			}
		}
		if(gridMap.heapSize()==0)
		{
			printf("No Path \n ");
			break;
		}
	}	
		
}

vector<Node> JPS::identifySuccessors(Node* node)
{
	successors.resize(8);
	neighbors=getNeighborsPrune(node); // all neighbors after pruned
	for(int i=0;i<neighbors.size();i++)
	{
		tmpXY=jump(neighbors[i][0],neighbors[i][1],node->x,node->y);//get the next jump point 
		if(tmpXY[0]!=-1)
		{
			int x=tmpXY[0];
			int y=tmpXY[1];
			ng=gridMap.toPointApprox(x,y,node->x,node->y)+node->g;
			if((gridMap.getNode(x,y)->f)<=0 || grid.getNode(x,y)>ng)
			{
				gridMap.getNode(x,y).updateGHFP(gridMap.toPointApprox(x,y,node->x,node->y)+
								node->g,gridMap.toPointApprox(x,y,endX,endY),node); // update G,H,F,P
				successors[i]=gridMap.getNode(x,y); // add this node to the successor list to be returned
			}
		}
	}
	return successors;
}

vector<int> JPS::jump(int x, int y, int px , int py)
{
	int jx[]={-1,-1}; // used to later check if full or null
	int jy[]={-1,-1}; // used to later check if full or null
	int dx=(x-px)/max(abs(x-px,1);
	int dy=(y-py)/max(abs(y-py,1);
	if(!gridMap.walkable(x,y))
	{
		tmpInt.resize(2);
		tmpInt[0]=-1;
		tmpInt[1]=-1;
		return tmpInt;
	}
	if(x==endX && y==endY)
	{
		tmpInt.resize(2);
		tmpInt[0]=x;
		tmpInt[1]=y;
		return tmpInt;
	}
	if(dx!=0 && dy!=0) // diagonally adjacent square ! caution !!!
	{
		if((gridMap.walkable(x-dx,y+dy) && !gridMap.walkable(x-dx,y)) || (gridMap.walkable(x+dx,y-dy) && !gridMap.walkable(x,y-dy)))
		{
			tmpInt.resize(2);	
			tmpInt[0]=x;
			tmpInt[1]=y;
			return tmpInt;
		}
	}
	else // check for horizontal and vertical
	{
		if(dx!=0)
		{
			if((gridMap.walkable(x+dx,y+1) && !gridMap.walkable(x,y+1)) || (gridMap.walkable(x+dx,y-1) && !gridMap.walkable(x,y-1)))
			{
				tmpInt.resize(2);
				tmpInt[0]=x;
				tmpInt[1]=y;
				return tmpInt;
			}
		}
		else
		{
			if(gridMap.walkable(x+1,y+dy) && !gridMap.wwalkable(x+1,y)) || (gridMap.walkable(x-1,y+dy) && !gridMap.walkable(x-1,y)))
			{
				tmpInt.resize(2);
				tmpInt[0]=x;
				tmpInt[1]=y;
				return tmpInt;
			}		
		}
	}
	if(dx!=0 && dy!=0)
	{
		jx=jump(x+dx,y,x,y);
		jy=jump(x,y+dy,x,y);
		if(jx[0]!= -1 || jy[0]!=-1)
		{
			tmpInt.resize(2);
			tmpInt[0]=x;
			tmpInt[1]=y;
			return tmpInt;	
		}	
	}
	if(gridMap.walkable(x+dx,y) || gridMap.walkable(x,y+dy))
	{
		return jump(x+dx,y+dy,x,y);
	}
	else 
	{
		tmpInt.resize(2);
		tmpInt[0]=-1;
		tmpInt[1]=-1;
		return tmpInt;
	}
}

// routine to convert tmpInt into vector of 2 elements 

vector<int>* JPS::getNeighborsPrune(Node *node(
{
	Node* parent=node->parent;
	int x=node->x;
	int y=node->y;
	int px,py,dx,dy;
	vector<int>* neighbors;
	neighbors=new vector<int>[5];
	for(int i=0;i<neighbors.size();i++)	
	{
		neighbors[i].resize(2);
	}
	if(parent!=NULL)
	{
		px=parent->x;
		py=parent->y;
		dx=(x-px)/max(abs(x-px),1);
		dx=(y-py)/max(abs(y-py),1);
		if(dx!=0 && dy!=0)
		{
			if(gridMap.walkable(x,y+dy))
			{
				neighbors[0][0]=x;
				neighbors[0][1]=y+dy;
			}
			if(gridMap.walkable(x+dx,y))
			{
				neighbors[1][0]=x+dx;
				neighbors[1][1]=y;
			}			
			// diagonal case 3
			// diagonal case 1
			// diagonal case 9
		}
		else
		{
			if(dx==0)
			{
				if(gridMap.walkable(x,y+dy))
				{
					if(gridMap.walkable(x+dx,y))
					{
						neighbors[0][0]=x+dx;
						neighbors[0][1]=y;
					}
					if(!gridMap.walkable(x,y+1))
					{
						neighbors[1][0]=x+dx;	
						neighbors[1][1]=y+1;
					}
					if(!gridMap.walkable(x,y-1))
					{
						neighbors[2][0]=x+dx;
						neighbors[2][1]=y-1;
					}
				}	
			}
		}
	}
	else
	{
		return gridMap.getNeighbors(node); // return all neighbors
	}
	return neighbors; // returns the neighbor candidates
}

/////////////////////////////////////////////////////////////////////////////
class Grid
{
	vector<vector<Node> > grid;
	int xMax,yMax,xMin,yMin;
	Heap* heap;
	public:	
		Grid(int xMaxx,int yMaxx); // xIsland,yIsland,uniform
		Grid(int xMaxx,int yMaxx,vector<Node>* gridd);
		vector<int>* getNeighbors(Node* node);
		bool walkable(int x,int y);
		float toPointApprox(float x,float y,int tx,int ty);
		Node* getNode(int x,int y);
		vector<int> getOpenPos();
		Node* heapPopNode();
		int heapSize();
		void heapAdd(Node *node);
}

Grid::Grid(int xMaxx,int yMaxx)
{
	xMax=xMaxx;
	yMax=yMaxx;
	xMin=yMin=0;
	vector< vector<Node> > gridLocal(xMax,vector<Node>(yMax));
	grid=gridLocal;
	Heap heap();
}

Grid::grid(int xMaxx,int yMaxx,vector< vector<Node> > gridd)
{
	xMax=xMaxx;
	yMax=yMaxx;
	xMin=yMin=0;
	grid=gridd;
	Heap heap();	
}

vector<vector<int> > Grid::getNeighbors(Node* node)
{
	vector<vector<int> > neighbors(8,vector<int>(2));
	int x=node->x;
	int y=node->y;
	bool d0=false;
	bool d1=false;
	bool d2=false;
	bool d3=false;
	if(walkable(x,y-1))
	{
		neighbors[0][0]=x;
		neighbors[0][1]=y-1;
		d0=d1=true;
	}
	if(walkable(x+1,y))
	{
		neighbors[1][0]=x+1;
		neighbors[1][1]=y;
		d1=d2=true;
	}
	if(walkable(x,y+1))
	{
		neighbors[2][0]=x;	
		neighbors[2][1]=y;
		d2=d3=true;
	}
	if(walkable(x-1,y))
	{
		neighbors[3][0]=x-1;
		neighbors[3][1]=y;
		d3=d0=true;
	}

	// ignoring the diagonal cases !!! robots can't move diagonally 
	return neighbors;
}

bool Grid::walkable(int x,int y)
{
	// implement accordingly !!!
	try
	{
		return getNode(x,y).pass;
	}
	catch(Exception e)
		return false;
}

float Grid::toPointApprox(float x,float y,int tx,int ty){return abs(x-tx)+abs(x-ty);}

Node* Grid::getNode(int x,int y){return grid[x][y];}

vector<int> Grid::getOpenPos(){while(1){int tA=rand()%xMax;int tB=rand()%yMax;if(walkable(tA,tB)){tmpInt[0]=tA;tmpInt[1]=tB;return tmpInt;}}}

Node* Grid::heapPopNode(){vector<float> tmp=heap.pop();return getNode((int)tmp[0],(int)tmp[1]);}

int Grid::heapSize(){return heap.getSize()}

void Grid::heapAdd(Node *node){vector<float> tmp(3);tmp[0]=node->x;tmp[1]=node->y;tmp[2]=node->f;}

/////////////////////////////////////////////////////////////////////////////
int main()
{
	int xMax=10;//xLimit
	int yMax=10;//yLimit
	vector< vector<Node> > grid(xMax,vector<Node>(yMax)); // 2 -D Grid as a vector of vectors
	gridInitializer(grid); // initialises the grid with current coordinates of bots and obstacles  
	JPS jpsGraph(xMax,yMax,grid);// Instance of JPS Class ... input : boundaries and Sortation Store Grid Map
	// time measurement from ctime utilities	
	jpsGraph.search(); // search for the initialised instance of JPS above ...
	// time measurement from ctime utilities

	// print the time difference as time for the computation of path through JFS Algo 

	return 0;
}

